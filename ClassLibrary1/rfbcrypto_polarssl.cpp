#include "Stdafx.h"
#include <string.h>
#ifndef IS_polarssl
#include <mbedtls\md5.h>
#include <mbedtls/sha1.h>
#else
#include <polarssl/md5.h>
#include <polarssl/sha1.h>
#endif // !IS_polarssl





#include "rfbcrypto.h"
#if defined( MBEDTLS_MD5_H)  && defined(MBEDTLS_SHA1_H)
void digestmd5(const struct iovec *iov, int iovcnt, void *dest)
{
	mbedtls_md5_context c;
	int i;

	mbedtls_md5_starts(&c);
	for (i = 0; i < iovcnt; i++)
		mbedtls_md5_update(&c, (const unsigned char *)iov[i].iov_base, iov[i].iov_len);
	mbedtls_md5_finish(&c, (unsigned char *)dest);
}

void digestsha1(const struct iovec *iov, int iovcnt, void *dest)
{
	mbedtls_sha1_context c;
	int i;

	mbedtls_sha1_starts(&c);
	for (i = 0; i < iovcnt; i++)
		mbedtls_sha1_update(&c, (const unsigned char *)iov[i].iov_base, iov[i].iov_len);
	mbedtls_sha1_finish(&c, (unsigned char *)dest);
}
#else
void digestmd5(const struct iovec *iov, int iovcnt, void *dest)
{

	md5_context c;
	int i;

	md5_starts(&c);
	for (i = 0; i < iovcnt; i++)
		md5_update(&c, iov[i].iov_base, iov[i].iov_len);
	md5_finish(&c, dest);
}

void digestsha1(const struct iovec *iov, int iovcnt, void *dest)
{
	sha1_context c;
	int i;

	sha1_starts(&c);
	for (i = 0; i < iovcnt; i++)
		sha1_update(&c, iov[i].iov_base, iov[i].iov_len);
	sha1_finish(&c, dest);
}



#endif // !IS_polarssl